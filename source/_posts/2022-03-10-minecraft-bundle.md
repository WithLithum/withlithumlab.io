---
title: Minecraft Java版和基岩版捆绑发售
date: 2022-03-10 10:53:54 +0800
tags:
- Minecraft
- BE
- 基岩版
- 新闻
---

最近这几天，Minecraft 官方给出了一个[重磅消息](https://help.minecraft.net/hc/en-us/articles/4607523809933-Buy-Minecraft-PC-Bundle-Get-Both-Games-) — Minecraft 基岩版和Java版将会_捆绑发售_。

同时，对于现有的 Java 版或基岩版玩家，只要你拥有任一版本，那么在捆绑包发售的时候（预计今年_北半球_夏天，也就是大约第2-3季度），你就可以在同一个账号上免费获取另一个版本；对于已经拥有两个版本的用户，对你没有任何影响。

注意此优惠并不适用于还在使用 Mojang 账户的用户 — 如果你想免费领到另一个版本，你就必须迁移到**微软账户**（说真的，微软账户防盗比老账户好太多，而且你也可以跟Windows使用同一账号）；同时，此优惠也不适用于非电脑平台的玩家（此处电脑平台指 Windows、GNU/Linux 和 Mac），也就是说你买了 Xbox 版是不会获得其它版本的。

## 领取方法

官方有提及领取方法：

> 在捆绑包发售之后，如果你有 PC Bundle 中两个版本的其中一个，那么你就可以到 Microsoft Store 里面免费领取新的 Minecraft PC Bundle。
