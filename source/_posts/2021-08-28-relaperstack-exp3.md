---
uuid: 15c56318-5588-2cc5-b33f-3894f9887f8f
layout: post
title:  "RelaperStack Local exp3 已发布"
date:   2021-08-28 14:43:00 +0800
tags: 
- 新闻
- 软件
thumbnail: /images/2022_03_relaperstack.png
---

欢迎来到 RelaperStack Local。这个存档将会为您提供一个完整的多人创造模式游戏环境，诸如安全系统、封禁。

## 注意事项

* 请注意此版本使用的是 1.17.1 版本，并不再兼容我的世界中国版。
* 您可以将此存档安装至一个服务器上。如需 24/7 运行，我们建议使用更多插件，但为确保体验，请尽量不要加入诸如领地、地皮、经济等插件。
* 此存档不适合所有除多人创造模式以外的场景。

## 新增功能

* 添加新指令 `whoami`、`blip`
* 使一部分指令使用最新的 Emoji 系统消息格式
* 使一部分指令向管理员通告其发送者和目标
* 未注册玩家现在会被关进小黑屋中，直到注册完毕
* 新增实体限制功能
* 升级至 1.17.1
* 控制台新增光源方块

## 下载安装

下载地址：[点我](https://gitlab.com/RelaperDev/download-holder/-/raw/main/relaperstack/RelaperStack.7z?inline=false)
