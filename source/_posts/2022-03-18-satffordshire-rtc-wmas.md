---
title: Staffordshire Moorlands 行人被撞重伤
date: 2022-03-18 19:35:18
tags:
- WMAS
- 急救
- 英国
categories: 搬运
thumbnail: https://i0.wp.com/wmas.nhs.uk/wp-content/uploads/2017/09/blue-lights-at-night-2.jpg?resize=900%2C420&ssl=1
license: 转载请注明原作者和本翻译作品，如有侵权，请联系删除。
---

> 原文：[Pedestrian seriously injured in Staffordshire Moorlands](https://wmas.nhs.uk/2022/03/18/pedestrian-seriously-injured-in-staffordshire-moorlands/)<br />原作者：West Midlands Ambulance Service University NHS Foundation Trust
> 
> 转载请注明原作者。

Claire Brown — 2022年3月18日 — 上午 9:10

一名行人昨晚被车撞伤。

West Midlands 急救中心昨晚在近晚七点时接到位于 Leek 路和 Cheddleton Heath 的警情。中心出动了一台急救车、一名指挥员，此外还有一名来自 North Staffordshire BASICS 的急救医生增援。

一名急救中心发言人说：

> “急救人员在现场发现了一名据报警人称被车撞伤的男子；热心群众和警察正在对其实施急救。经急救小组判断，男子生命垂危。急救人员立即对该男子开展创伤急救程序，并将其一路送至斯托克大学皇家医院继续抢救。
